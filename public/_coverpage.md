![Logo](logo.svg)

# Shedinja <small style="font-family: var(--base-font-family);">0.0.5-SNAPSHOT</small>

> Dependency injection library for Kotlin.

Simple, safe, easy-to-use and flexible.

> **Shedinja is deprecated.** Shedinja has been moved to the [Tegral project](https://tegral.zoroark.guru) under the name Tegral DI. Check it out [here](https://tegral.zoroark.guru/docs/core/di)!

[Documentation](/#shedinja-main) [GitHub](https://github.com/utybo/Shedinja)

![](bg.jpg)
